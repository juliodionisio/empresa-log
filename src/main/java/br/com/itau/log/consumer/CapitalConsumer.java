package br.com.itau.log.consumer;

import br.com.itau.capital.producer.Capital;
import com.opencsv.CSVWriter;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class CapitalConsumer {

    @KafkaListener(topics = "spec3-julio-felipe-3", groupId = "gazorpazorp")
    public void receber(@Payload Capital capital) throws IOException {
        System.out.println("Recebi a empresa " + capital.getNome() + ", com CNPJ " + capital.getCnpj() + ", com capital de " + capital.getCapital_social());

        String[] log = {capital.getNome(), capital.getCnpj(), Double.toString(capital.getCapital_social())};
        FileWriter fw = new FileWriter("empresasaceitas.csv", true);
        CSVWriter csvWriter = new CSVWriter(fw);

        csvWriter.writeNext(log);

        csvWriter.flush();
        fw.close();

    }

}
